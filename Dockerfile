FROM node:9-alpine

RUN npm install -g spa-http-server

COPY ./build /app

EXPOSE 8080

CMD ["http-server","app/","-S","-C","app/keys/cert.pem","-K","app/keys/privkey.pem","--push-state"]
