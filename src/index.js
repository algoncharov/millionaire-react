import React             from 'react'
import { Provider }      from 'react-redux'
import { render }        from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import thunk             from 'redux-thunk'
import App               from './components/App'
import reducer           from './reducers/index'
import { check_token }   from './actions/auth'
import                        './style/index.css'
import { 
  createStore, 
  applyMiddleware,
  compose  
} from 'redux'

export  const rest_api_host = 'https://milioner.ddns.net:8092'
// export const rest_api_host = 'https://localhost:8443'

// localStorage.clear() // debug only, delete in production
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)))
store.dispatch(check_token())

render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
      </BrowserRouter>
    </Provider>    
  ,document.getElementById('root')
)
