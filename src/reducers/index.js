import pollRes from '../asserts/music/classic_help3_results.mp3'
import sound5050 from '../asserts/music/classic_help1.mp3'
import timer from '../asserts/music/timer.mp3'
import help3 from '../asserts/music/preliminary.mp3'

import      q1 from '../asserts/music/classic_q1.mp3'
import      q6 from '../asserts/music/classic_q6.mp3'
import      q7 from '../asserts/music/classic_q7.mp3'
import      q8 from '../asserts/music/classic_q8.mp3'
import      q9 from '../asserts/music/classic_q9.mp3'
import     q10 from '../asserts/music/classic_q10.mp3'
import     q11 from '../asserts/music/classic_q11.mp3'
import     q12 from '../asserts/music/classic_q12.mp3'
import     q13 from '../asserts/music/classic_q13.mp3'
import     q14 from '../asserts/music/classic_q14.mp3'
import     q15 from '../asserts/music/classic_q15.mp3'

import  right1 from '../asserts/music/classic_r1.mp3'
import  right5 from '../asserts/music/classic_r5.mp3'
import  right6 from '../asserts/music/classic_r6.mp3'
import  right7 from '../asserts/music/classic_r7.mp3'
import  right8 from '../asserts/music/classic_r8.mp3'
import  right9 from '../asserts/music/classic_r9.mp3'
import right10 from '../asserts/music/classic_r10.mp3'
import right11 from '../asserts/music/classic_r11.mp3'
import right12 from '../asserts/music/classic_r12.mp3'
import right13 from '../asserts/music/classic_r13.mp3'
import right14 from '../asserts/music/classic_r14.mp3'
import right15 from '../asserts/music/classic_r15.mp3'

import  wrong1 from '../asserts/music/classic_w1.mp3'
import  wrong6 from '../asserts/music/classic_w6.mp3'
import  wrong7 from '../asserts/music/classic_w7.mp3'
import  wrong8 from '../asserts/music/classic_w8.mp3'
import  wrong9 from '../asserts/music/classic_w9.mp3'
import wrong10 from '../asserts/music/classic_w10.mp3'
import wrong11 from '../asserts/music/classic_w11.mp3'
import wrong12 from '../asserts/music/classic_w12.mp3'
import wrong13 from '../asserts/music/classic_w13.mp3'
import wrong14 from '../asserts/music/classic_w14.mp3'
import wrong15 from '../asserts/music/classic_w15.mp3' 

const initialState = {
  id: '2',
  question: {
    'id': 1,
    'text': '',
    'answerDTOs': [
      { 'id': 1, 'text': '' },
      { 'id': 2, 'text': '' },
      { 'id': 3, 'text': '' },
      { 'id': 4, 'text': '' },
    ]
  },
  question1: {
    'answerDTOs': [
      { 'id': 1, 'text': '' },
      { 'id': 2, 'text': '' },
    ]
  },
  question2: {
    "answers": [
      { "id": 1, "percent": '' },
      { "id": 2, "percent": '' },
      { "id": 3, "percent": '' },
      { "id": 4, "percent": '' }
    ]
  },
  sync: false,
  loading: false,
  show_result: false,
  clicked_id: 1231242352,
  normal_player_click: false,
  result: false,
  bgSound: q1,
  bgSoundStatus: true,
  actSound: null,
  actSoundStatus: false,
  used5050: false,
  used50501: false,
  usedPoll: false,
  usedPoll1: false,
  usedTimer: false,
  usedTimer1: false,
  logged_in: undefined, // todo make sub-object for login states
  login_fail_alert: false,
  login_redirectToReferrer: false,
  username: '',
  role: '',
  userId:'',
  roomId: 0,
  roomList: [],
  userList: [],
  userRoleList: [],
  activeRoom: null,
  questionList: [],
  categoryesList: [],
  joinedRoomId: null,
  roomUserList:[],
  gameStarted: false, // you can be in room but wait for game start
  hostId: null,
  activePlayerId: 0,
  preliminary: false,
  prelimCorrectArray: null,
  prelimPlayers: null,
  answerArray: [
      { "answerId": 0, "ord": 0 },
      { "answerId": 0, "ord": 0 },
      { "answerId": 0, "ord": 0 },
      { "answerId": 0, "ord": 0 }
    ],
  index: 0,
  rezArray:[
      { "rez": 0 },
      { "rez": 0 },
      { "rez": 0 },
      { "rez": 0 }
      ],
  questionUpdate: {
    'id': '',
    'category': '',
    'text': '',
    'answer1': '',
    'answer2': '',
    'answer3': '',
    'answer4': '',
  },
  categoryesList1: [
  {'ord':0},
  {'ord':1},
  {'ord':2},
  {'ord':3},
  {'ord':4},
  {'ord':5},
  {'ord':6},
  {'ord':7},
  {'ord':8},
  {'ord':9},
  {'ord':10},
  {'ord':11},
  {'ord':12},
  {'ord':13},
  {'ord':14},
  {'ord':-1},
  ]
}
// localStorage.getItem('access_token')
export default (state=initialState, action) => {
  switch (action.type) {
    case 'PRELIMINARY_ANSWER_CLICKED':
    console.log(action.correctAnswerPairs)
      return Object.assign({}, state, {
        rezArray: state.rezArray.map((rez, index) => {
          if((action.answerArray[index].ord === action.correctAnswerPairs[index].ord) &&
            (action.answerArray[index].answerId === action.correctAnswerPairs[index].answerId)) {
            return Object.assign({}, rez, {
              rez: true,
            })
          }
          return rez
        })
      })
      // { ...state, 
      //          rez: (() => {
      //          for (let i = 0; i<4; i++){
      //           if ((action.answerArray[i].ord === action.correctAnswerPairs[i].ord) &&
      //             (action.answerArray[i].answerId === action.correctAnswerPairs[i].answerId)) {
      //               rez1: state.rez1+1
      //             }
      //          }}),
      //       }
    case 'TO_ARRAY':
      return Object.assign({}, state, {
        show_result:true,
        index: state.index+1,
        answerArray: state.answerArray.map((answer, index) => {
          if(index === state.index) {
            return Object.assign({}, answer, {
              answerId: action.clicked_id,
              ord: index+1
            })
          }
          return answer
        })
      })
    case 'ANSWER_CLICKED':
      return { 
        ...state, 
        normal_player_click: false,
        show_result: true, 
        clicked_id: state.normal_player_click ? state.clicked_id : action.clicked_id, 
        result: action.result, 
        actSound: action.result === action.clicked_id? getRightSound(state.question.id) : getWrongSound(state.question.id),
      }
    case 'NORMAL_PLAYER_ANSWER_CLICKED':
          return { 
        ...state, 
        show_result: true, 
        clicked_id: action.clicked_id, 
        normal_player_click: true
      }
    case 'REQUEST_POSTS':
      return { ...state, loading:true }
    case 'RECEIVE_POSTS':
      return { 
        ...state, 
        index: 0,
        usedTimer: false, 
        usedPoll1: false, 
        used50501: false, 
        loading:false, 
        question:action.question, 
        bgSound: action.question.preliminary ? help3 : getQuestionSound(state.question.id), 
        actSound: null,
        show_result: false,
        preliminary: action.question.preliminary,
        prelimCorrectArray:null,
        answerArray: [
      { "answerId": 0, "ord": 0 },
      { "answerId": 0, "ord": 0 },
      { "answerId": 0, "ord": 0 },
      { "answerId": 0, "ord": 0 }
    ],
        // hostId: state.userId, //hardcode

      }
    case 'RECEIVE_POSTS1':
      return { 
        ...state, 
        usedTimer: false, 
        usedTimer1: false, 
        usedPoll1: false, 
        usedPoll: false, 
        used50501: false, 
        used5050: false, 
        loading:false, 
        question:action.question, 
        bgSound: q1, 
        actSound: null,
        show_result: false,
        
      }
    case 'RECEIVE_POSTS_HINT_5050':
      return { 
        ...state, 
        usedTimer: false, 
        used50501: true, 
        used5050: true, 
        loading:false, 
        question1:action.question, 
        actSound: sound5050, 
        show_result: false
      }
    case 'RECEIVE_POSTS_HINT_POLL':
    return {
      ...state, 
      usedTimer: false, 
      usedPoll1: true, 
      usedPoll: true, 
      loading:false, 
      question2:action.question, 
      actSound: pollRes, 
      show_result: false
    }
    case 'RECEIVE_POSTS_HINT_CALL':
      return {
        ...state, 
        usedTimer: true, 
        usedTimer1: true, 
        actSound: timer,
      }
    case 'GET_ONE_ROOM':
    console.log('ACTION:', action)
      return {
        ...state, 
        activePlayerId: action.room.activePlayer,
        hostId: action.room.host,
        roomUserList: action.room.users,
      } 
    case 'UPDATED_QUESTION':
      return {
        ...state, 
        questionUpdateid: {
        id: action.id,
        category: action.category,
        text: action.question,
        answer1: action.answer1,
        answer2: action.answer2,
        answer3: action.answer3,
        answer4: action.answer4,
      }}
    case 'GET_CATEGORYES_LIST':
      return { ...state, categoryesList: action.categoryList } 
    case 'SET_PRELIIM_RESULTS':
        return {
        ...state, 
        prelimCorrectArray: action.res.correctAnswerPairs,
        prelimPlayers: action.res.prelimPlayers,
        loading: false,
      } 
    case 'LOG_IN':
      return { ...state, logged_in: true, username: action.username, role: action.role, userId:+action.userId  }
    case 'DO_REDIRECT_TO_REFERRER':
      return { ...state, login_redirectToReferrer: true }    
    case 'REDIRECT_TO_REFERRER_FALSE':
      return { ...state, login_redirectToReferrer: false }    
    case 'SHOW_LOGIN_FAIL':
      return { ...state, login_fail_alert: true }
    case 'HIDE_LOGIN_FAIL':
      return { ...state, login_fail_alert: false } 
    case 'LOG_OUT':
      return { ...initialState, logged_in: false }
    case 'UPDATE_ROOM_LIST':
      return { ...state, roomList: action.roomList } 
    case 'SET_JOINED_ROOM_ID':
      return { ...state, joinedRoomId: action.roomId }
    case 'START_GAME':
      return { ...state, gameStarted: true }
    case 'STOP_GAME':
      return { ...state, gameStarted: false }
    case 'UPDATE_USER_LIST':
      return { ...state, userList: action.userList }  
    case 'GET_USER_ROLE_LIST':
      return { ...state, userRoleList: action.userRoleList }    
    case 'REDIRECT_TO_ROOM':
      return { ...state, roomToRedirect: action.roomId}
    case 'SYNC_START':
      return { ...state, sync: true}
    case 'SYNC_COMPLETE':
      return { ...state, sync: false}
    case 'UPDATE_QUESTION_LIST':
      return { ...state, questionList: action.questionList } 
    default:
      return state
  }
}


const getRightSound = (answer_num) => {
  switch (answer_num) {
    case 1:
    case 2:
    case 3:
    case 4:  return right1; 
    case 5:  return right5; 
    case 6:  return right6; 
    case 7:  return right7; 
    case 8:  return right8; 
    case 9:  return right9; 
    case 10: return right10; 
    case 11: return right11; 
    case 12: return right12; 
    case 13: return right13;
    case 14: return right14; 
    case 15: return right15; 
    default: return right1;
  }
}

const getWrongSound = (answer_num) => {
  switch (answer_num) {
    case 1:
    case 2:
    case 3:
    case 4: 
    case 5:  return wrong1; 
    case 6:  return wrong6; 
    case 7:  return wrong7; 
    case 8:  return wrong8; 
    case 9:  return wrong9; 
    case 10: return wrong10; 
    case 11: return wrong11; 
    case 12: return wrong12; 
    case 13: return wrong13; 
    case 14: return wrong14; 
    case 15: return wrong15; 
    default: return wrong1;
  }
}

const getQuestionSound = (answer_num) => {
  switch (answer_num) {
    case 1:
    case 2:
    case 3:
    case 4:  return q1
    case 5:   
    case 6:  return q6; 
    case 7:  return q7; 
    case 8:  return q8; 
    case 9:  return q9; 
    case 10: return q10; 
    case 11: return q11; 
    case 12: return q12; 
    case 13: return q13; 
    case 14: return q14; 
    case 15: return q15; 
    default: return q1;
  }
}
