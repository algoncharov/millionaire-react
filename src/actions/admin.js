import { rest_api_host } from '../index'
import { requestPosts } from './game_fetches'
import { check_token, doRedirectToReferrer } from './auth'

const updateUserList = (userList) => ({
  type: 'UPDATE_USER_LIST',
  userList
})

export const updatedUser = (user) => {
  return ({
  type: 'UPDATED_USER',
  user
})}

export const checkAuthority = () => {
  return dispatch => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { headers: { 'access_token': token }}
      return fetch(rest_api_host + '/admin/**', opts)
        .then(response => {
          if (response.ok) {
            console.log('ok')
          } 
          else {
            console.log('fuck you')
            dispatch(doRedirectToReferrer())
          }
        })
        .catch(err => { console.log('Fetch Error in checkAuthority():', err) })
    }
  }
}

export const loadUserList = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { 
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
      }
      dispatch(checkAuthority())
      return fetch(rest_api_host + '/api/users/', opts)
        .then(response => {
          if (response.ok) { 
            return response.json() 
          }
          else { dispatch(check_token()) }          
        })
        .then(userListJson => { 
          console.log(userListJson)
          dispatch(updateUserList(userListJson)) 
        })
    }
  }
}


export const deleteUser = (userId) => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string   
    console.log(token) 
    if (token) {
      let opts = { 
        method: 'delete',
        headers: { 

          'access_token': token,
        },
      }
      
      fetch(rest_api_host + '/api/users/' + userId, opts)
        .then(() => dispatch(loadUserList()))
    }
  }
}

export const updateUser = (userId, user) => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string   
    if (token) {
      let opts = { 
        method: 'put',
        headers: { 
          'content-type': 'application/json',
          'access_token': token,
        },
        body: JSON.stringify({
          name: user.name,
          surname:user.surname,
          userRoleId: user.userRoleId
        })
      }
      
      fetch(rest_api_host + '/api/users/' + userId, opts)
        .then(response => {
          if (response.ok) {
            alert('Updated')
          } else {
            console.log('fuck')
          }
        })
    }
  }
}
const getUserRoleList = userRoleList => ({
  type: 'GET_USER_ROLE_LIST',
  userRoleList
})


export const getUserRoles = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { 
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
      }
      return fetch(rest_api_host + '/api/user_roles/', opts)
        .then(response => {
          if (response.ok) { return response.json() }
          else { dispatch(check_token()) }          
        })
        .then(userRoleList => { 
          dispatch(getUserRoleList(userRoleList)) })
    }
  }
}


export const saveQuestion = (category, question, answer1, answer2, answer3, answer4) => {
  return dispatch => {
    let token = localStorage.getItem('access_token')
    if (token) {
    let opts = { 
      method: 'POST',
      headers: { 
        'content-type': 'application/json',
        'access_token': token,
      },
      body: JSON.stringify({ 
        'ord': category,
    'text': question, 
    'answerDTOs': [ 
    {'id': 1, 
    'text': answer1}, 
    {'id': 2, 
    'text': answer2}, 
    {'id': 3, 
    'text': answer3}, 
    {'id': 4, 
    'text': answer4}] 
    })
  }

   dispatch(requestPosts())

    // let url = rest_api_host + '/login?username=' + username + '&pass=' + password
    console.log('SEEEND', opts.body)
    // return fetch(rest_api_host + '/login?username=user&pass=qwerty123')
    fetch(rest_api_host + '/api/questions', opts)
      .then(response => {
        if (response.ok) {
          alert('successfully')
        }
        else {
        alert('Wrong')
          console.log('Looks like there was a problem. Status Code: ' + response.status)
        }
      })
  }
}
}


const updateQuestionList = (questionList) => ({
  type: 'UPDATE_QUESTION_LIST',
  questionList
})

export const loadQuestionList = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      console.log('from question list')
      let opts = { 
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
      }
      dispatch(checkAuthority())
      return fetch(rest_api_host + '/api/questions', opts)
        .then(response => {
          if (response.ok) { return response.json() }
          else { dispatch(check_token()) }          
        })
        .then(questionListJson => { dispatch(updateQuestionList(questionListJson)) })
    }
  }
}


export const deleteQuest = (questionId) => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token')
    if (token) {
    let opts = { 
      method: 'DELETE',
      headers: { 
       
        'access_token': token
      },
     
  }

    // let url = rest_api_host + '/login?username=' + username + '&pass=' + password
    console.log('SEEEND', opts.body)
    // return fetch(rest_api_host + '/login?username=user&pass=qwerty123')
    fetch(rest_api_host + '/api/questions/' + questionId, opts)
      .then(response => {
        if (response.ok) {
          alert('successfully')
          dispatch(loadQuestionList());
        }
        else {
          alert('Wrong')
          console.log('Looks like there was a problem. Status Code: ' + response.status)
        }
      })
  }
}
}


export const getCategory = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      console.log('from question list')
      let opts = { 
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
      }

      return fetch(rest_api_host + '/api/questions/category', opts)
        .then(response => {
          if (response.ok) { return response.json() }
          else { dispatch(check_token()) }          
        })
        .then(categoryList => { dispatch(getCategoryesList(categoryList)) })
    }
  }
}

const getCategoryesList = categoryList => ({
  type: 'GET_CATEGORYES_LIST',
  categoryList
})

export const updatedQuestion = (id, category, question, answer1, answer2, answer3, answer4) => ({
  type: 'UPDATED_QUESTION',
  id, category, question, answer1, answer2, answer3, answer4
})

export const updateQuest = (id, category, question, answer1, answer2, answer3, answer4) => {
  return dispatch => {
    let token = localStorage.getItem('access_token')
    if (token) {
    let opts = { 
      method: 'POST',
      headers: { 
        'content-type': 'application/json',
        'access_token': token,
      },
      body: JSON.stringify({
      'id': id,
      'ord': category,
      'text': question, 
      'answerDTOs': [ 
      {'id': 1, 
      'text': answer1}, 
      {'id': 2, 
      'text': answer2}, 
      {'id': 3, 
      'text': answer3}, 
      {'id': 4, 
      'text': answer4}] 
      })
    }

    console.log('SEEEND', opts.body)
    fetch(rest_api_host + '/api/questions/update', opts)
      .then(response => {
        if (response.ok) {
          alert('successfully')
          // dispatch(loadQuestionList());
        }
        else {
        alert('Wrong')
        }
      })
  }
}
}
