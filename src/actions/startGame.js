import { rest_api_host } from '../index'
import { check_token } from './auth'
import { fetchQuestion } from './game_fetches'

export const startGame = () => ({
  type: 'START_GAME',
})

export const stopGame = () => ({
  type: 'STOP_GAME',
})

const startGameServer = roomId => {
  
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { method: 'POST', headers: { 'access_token': token }}
      
      return fetch(rest_api_host + '/api/rooms/' + roomId + '/start', opts)
        .then(response => {
          if (response.ok) {
            dispatch(fetchQuestion())
            dispatch(startGame())
          }
          else {
            dispatch(check_token())
          }
        })
    }
  }
}

export default startGameServer
