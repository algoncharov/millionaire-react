import { rest_api_host } from '../index'
import { check_token } from './auth'

export const requestPosts=()=> ({
  type: 'REQUEST_POSTS',
})

const receivePosts = (question) => {
  console.log('ПРИШЕЛ НОВЫЙ ВОПРОС', question)
  return({
  type: 'RECEIVE_POSTS',
  question
})}

export const receivePostsHint5050 = (question) => ({
  type: 'RECEIVE_POSTS_HINT_5050',
  question
})

export const receivePostsHintPoll = (question) => ({
  type: 'RECEIVE_POSTS_HINT_POLL',
  question
})
  
export const receivePostsHintCall = () => ({
  type: 'RECEIVE_POSTS_HINT_CALL'
})

const createRoom = room => ({
  type: 'CREATE_ROOM',
  room
})

export const getOneRoom = room => ({
  type: 'GET_ONE_ROOM',
  room
})


export const toArrayAnswers = clicked_id => ({
  type: 'TO_ARRAY',
  clicked_id
})

export const fetchRoom = () => {
  return dispatch => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { method: 'post', headers: { 'access_token': token }}
      dispatch(requestPosts())

      return fetch(rest_api_host + '/api/rooms', opts)
        .then(response => {
          if (response.ok) {
            return response.json()
          }
          else {
            dispatch(check_token())
          }
        })
        .then(room => dispatch(createRoom(room)))
    }
  }
}
// 
export const fetchOneRoom = roomId => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      console.log('have token')
      
      let opts = { headers: { 'access_token': token }}

      //dispatch(requestPosts())
      return fetch(rest_api_host + '/api/rooms/' + roomId, opts)
        .then(response => {
          if (response.ok) { return response.json() }
          else { 
            dispatch(check_token()) 
          }
        })
        .then(room => dispatch(getOneRoom(room)))
    }
  }
}

export const fetchQuestion = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { headers: { 'access_token': token }}

      dispatch(requestPosts())
      const {joinedRoomId} = getState()
      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/question', opts)
        .then(response => {
          if (response.ok) { return response.json() }
          else { 
            dispatch(check_token()) 
          }
        })
        .then(question => dispatch(receivePosts(question)))
    }
  }
}
export const continuePrelim = () =>{
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { 
        method: 'PATCH',
        headers: { 
          'Content-Type': 'application/json',          
          'access_token': token,
        },
        body: JSON.stringify({ 'continuePrelim': true })
      }
      const {joinedRoomId} = getState()
      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId, opts)
    }
  }
}

export const fetchQuestionHint5050 = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { headers: { 'access_token': token }}
      dispatch(requestPosts())
      const {joinedRoomId} = getState()

      return fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/question/hint/5050', opts)
        .then(response => {
          if (!response.ok) {
            dispatch(check_token())
          }
        })
        //.then(question => dispatch(receivePostsHint(question)))
    }
  }
}

export const fetchQuestionHintPoll = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { headers: { 'access_token': token }}
      dispatch(requestPosts())
      const {joinedRoomId} = getState()
      
      return fetch(rest_api_host + '/api/rooms/' +  joinedRoomId + '/question/hint/poll', opts)
        .then(response => {
          if (!response.ok) {
            dispatch(check_token())
          }
        })
        //.then(question => dispatch(receivePostsHintPoll(question)))
    }
  }
}

export const fetchQuestionHintCall = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { headers: { 'access_token': token }}
      const {joinedRoomId} = getState()
      
      return fetch(rest_api_host + '/api/rooms/' +  joinedRoomId + '/question/hint/call', opts)
        .then(response => {
          if (!response.ok) {
            dispatch(check_token())
          }
        })
        //.then(question => dispatch(receivePostsHintPoll(question)))
    }
  }
}

export const preliminaryAnswerClicked = (answerArray, correctAnswerPairs) => {
return { type: 'PRELIMINARY_ANSWER_CLICKED', answerArray, correctAnswerPairs }
}

export const answerClicked = (clicked_id, result) => {
  return { type: 'ANSWER_CLICKED', clicked_id, result }
}

export const fetchPreliminaryAnswer = () => {
  console.log('lalalala')
  return (dispatch, getState) => {
    const {joinedRoomId, answerArray} = getState()     
    let token = localStorage.getItem('access_token') // null or string    
    if (token) {
      let opts = { 
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',          
          'access_token': token,
        },
        body: JSON.stringify({ 'answerPairs': answerArray })
      }
      
      
      dispatch(requestPosts())
      
      fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/answerid', opts)
        .then(response => {
          if (!response.ok) {
            dispatch(check_token())
          }
        })
        //.then(data => dispatch(preliminaryAnswerClicked(answerArray, data.correctAnswerPairs)))
        //.then(setTimeout(() => dispatch(fetchQuestion()), 5000))
    }
  }
}

export const fetchAnswer = (clicked_id) => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) {
      let opts = { 
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',          
          'access_token': token,
        },
        body: JSON.stringify({ 'answerId': clicked_id })
      }
      
      
      const {joinedRoomId, userId, hostId, activePlayerId, normal_player_click} = getState()
      if (userId !== hostId && userId !== activePlayerId && !normal_player_click) {
        dispatch(normalPlayerAnswerClicked(clicked_id))
      }
      if (!normal_player_click) { //always false except when the normal player has already clicked one of the answers
        fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/answerid', opts)
          .then(response => {
            if (!response.ok) {
              dispatch(check_token())
            }
          })
      }
        //.then(data => dispatch(answerClicked(clicked_id, data.correct)))
        //.then(setTimeout(() => dispatch(fetchQuestion()), 5000))
    }
  }
}

const normalPlayerAnswerClicked = (clicked_id) => ({
  type: 'NORMAL_PLAYER_ANSWER_CLICKED',
  clicked_id
})
export const playAgain = (clicked_id) => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) {
      let opts = { 
        method: 'post',
        headers: { 
          'Content-Type': 'application/json',          
          'access_token': token,
        },
        body: JSON.stringify({ 'answerId': clicked_id })
      }
      const {joinedRoomId} = getState()
      dispatch(requestPosts())
     
      fetch(rest_api_host + '/api/rooms/' + joinedRoomId + '/answerid', opts)
        .then(response => {
          if (response.ok) {
            return response.json()
          }
          else {
            dispatch(check_token())
          }
        })
        //.then(data => dispatch(answerClicked(clicked_id, data.correct)))
        .then(() => dispatch(fetchQuestion()), 3000)
    }
  }
}

export const updateRoomList = roomList => ({
  type: 'UPDATE_ROOM_LIST',
  roomList
})

export const loadRoomList = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      console.log('fdd')
      let opts = { 
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          'access_token': token,
        },
      }

      return fetch(rest_api_host + '/api/rooms/', opts)
        .then(response => {
          if (response.ok) { return response.json() }
          else { dispatch(check_token()) }          
        })
        .then(roomListJson => { dispatch(updateRoomList(roomListJson)) })
    }
  }
}

export const setJoinedRoomId = roomId => ({
  type: 'SET_JOINED_ROOM_ID',
  roomId
})

export const setPrelimResults = res => ({
  type: 'SET_PRELIIM_RESULTS',
  res
})