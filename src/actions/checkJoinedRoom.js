import { rest_api_host } from '../index'
import { check_token } from './auth'
import { setJoinedRoomId } from './game_fetches'

const checkJoinedRoom = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { method: 'GET', headers: { 'access_token': token } }

      return fetch(rest_api_host + '/api/users/' + getState().userId + '/room', opts)
        .then(response => {
          // if (response.ok) { return response.json() }
          if (response.ok) { 
            return response.text()
          }
          else { dispatch(check_token()) }          
        })
        .then(room => {
          let roomInt = parseInt(room, 10)
          console.log(roomInt)
          if (roomInt > 0) {
            dispatch(setJoinedRoomId(roomInt))
          }
        })
    }
  }
}

export default checkJoinedRoom
