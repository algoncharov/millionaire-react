import { rest_api_host } from '../index'
import { check_token } from './auth'
import { updateRoomList, setJoinedRoomId, getOneRoom} from './game_fetches'
import { stopGame } from './startGame'
import { syncComplete } from './sync';
import redirectToRoom from './redirectToRoom'

const newRoom = () => {
  return (dispatch, getState) => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { method: 'POST', headers: { 'access_token': token }}


      /*eslint-disable */
      let joinedRoomId = getState().joinedRoomId
      if (joinedRoomId) {
        if (confirm(`Do you wanna quit room ${joinedRoomId}?`)) {
          dispatch(stopGame())
          dispatch(setJoinedRoomId(null))
        }
        else {
          return
        }
      }
      /*eslint-enable */
      
      return fetch(rest_api_host + '/api/rooms', opts)
        .then(response => {
          if (response.ok) { return response.json() }
          else { dispatch(check_token()) }
        })
        .then(room => { 
          dispatch(updateRoomList(getState().roomList.concat(room)))
          dispatch(setJoinedRoomId(room.id)) 
          // dispatch(syncComplete())
          // array1.concat(array2)
          dispatch(redirectToRoom(room.id))
          dispatch(syncComplete())
          dispatch(redirectToRoom(null))
          // history.push('/room/12323123')
          dispatch(getOneRoom(room));
        })
    }
  }
}

export default newRoom
