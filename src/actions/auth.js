import { rest_api_host } from '../index'

export const log_in = token => {
  // parse JWT for username and role
  let base64Url = token.split('.')[1];
  let base64 = base64Url.replace('-', '+').replace('_', '/');
  let jwt_json = JSON.parse(window.atob(base64))
  console.log('USER',jwt_json)
  return { 
    type: 'LOG_IN',
    username: jwt_json.sub,
    userId: jwt_json.user_id,
    role: jwt_json.authorities, // TODO: this can be List (many roles), it should be handled
  }
}

export const log_out = () => {
  localStorage.clear()
  return { 
    type: 'LOG_OUT'
  }
}

export const show_login_fail = () => {
  return { 
    type: 'SHOW_LOGIN_FAIL'
  }
}

export const hide_login_fail = () => {
  return { 
    type: 'HIDE_LOGIN_FAIL'
  }
}

export const doRedirectToReferrer = () => {
  return { 
    type: 'DO_REDIRECT_TO_REFERRER'
  }
}

export const redirectToReferrerFalse = () => {
  return { 
    type: 'REDIRECT_TO_REFERRER_FALSE'
  }
}

/**
 * this action trys to send username and password to server 
 * if server responds OK then UI switches to logged_in state
 */
export const try_log_in = (username, password) => {
  return dispatch => {
    
    let opts = { 
      method: 'POST',
      headers: { 
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        'username': username,
        'password': password,
      }),
    }



    // let url = rest_api_host + '/login?username=' + username + '&pass=' + password
    // console.log('fetching url:', url)
    // return fetch(rest_api_host + '/login?username=user&pass=qwerty123')
    return fetch(rest_api_host + '/login', opts)
      .then(response => {
        if (response.ok) {
          let token = response.headers.get('access_token')
          localStorage.setItem('access_token', token)
          dispatch(log_in(token))
          dispatch(doRedirectToReferrer())
        }
        else {
          console.log('Looks like there was a problem. Status Code: ' + response.status)
          dispatch(show_login_fail())
          setTimeout(() => {
            dispatch(hide_login_fail())
          }, 2000)
        }
      })
      .catch(err => { console.log('Fetch Error in try_log_in():', err) })
  }
}


export const signup = (username, lastname, password) => {
  return dispatch => {
    
    let opts = { 
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        "name" : username,
        "surname" : lastname,
        "password" : password,
      })
    }

    return fetch(rest_api_host + '/api/users', opts)
      .then(response => {
        if (response.ok) {
          dispatch(try_log_in(username, password))
          // dispatch(doRedirectToReferrer())
        }
        else {
          console.log('Looks like there was a problem. Status Code: ' + response.status)
          dispatch(show_login_fail())
          setTimeout(() => {
            dispatch(hide_login_fail())
          }, 2000)
        }
      })
      .catch(err => { console.log('Fetch Error in try_log_in():', err) })
  }
}


/**
 * checks does user have token in localStorage?
 * if he/she does then sends request to /login_check to see is it valid
 */
export const check_token = () => {
  return dispatch => {
    let token = localStorage.getItem('access_token') // null or string    
    if (token) { 
      let opts = { headers: { 'access_token': token }}
      return fetch(rest_api_host + '/login_check', opts)
        .then(response => {
          if (response.ok) {
            dispatch(log_in(token))
          } 
          else {
            dispatch(log_out())
          }
        })
        .catch(err => { console.log('Fetch Error in loginCheck():', err) })
    }
    dispatch(log_out())
  }
}
