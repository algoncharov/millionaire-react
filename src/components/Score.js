import React from 'react'
import Category from './Category'
import '../style/score.css'

const Score = ({active}) => {
      var money = ['1 000 000', '500 000','250 000', '125 000','64 000','32 000','16 000','8 000','4 000','2 000','1 000','500','300','200','100']
      var index = 16;
      return (
        <table className="scorebar">
        <tbody>
            {money.map(category =>
                <Category 
                  key = {index = index-1}
                  index={index} 
                  sum={category}
                  active={active}
                  className = {active === index ? 'active': ''}
                />
              )
            }
          </tbody>
        </table>
    )
}

export default Score