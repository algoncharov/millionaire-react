import React from 'react';

const Answer = ({ userId, activePlayerId, text, letter, onClick , className, isActive,style}) => 
	<div onClick={ isActive ? onClick : null} 
		style={style} 
		className = {'answer ' + className }
	> 
		<p><span>{letter + ': '}</span>{ isActive ? text : null }</p>
	</div>

export default Answer
