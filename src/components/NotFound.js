import React from 'react';

const NotFound = () => 
  <h1 
    style={{
      'color':'white',
      'font-size': '10rem',
    }
  }>
    404 Page Not Found
  </h1>

export default NotFound
