import React from 'react';
import '../style/timer.css'

class Timer extends React.Component{
    state = {
        elapsed: this.props.start
    }

    componentDidMount() {
        this.timer = setInterval(this.tick, 1000);
    }

    componentWillUnmount() {
         clearInterval(this.timer);
    }

    tick = () => {
        this.setState({elapsed: this.props.used ? ((this.state.elapsed >= 0) ? this.state.elapsed-1 : -1) : 30});
    }

    render() {
        var elapsed =  this.state.elapsed ;
         return (
            <div className="timer" style={((this.state.elapsed < 0) || (!this.props.used)) ? {display: 'none'} : {}}>
                <span>{elapsed}</span>
            </div>
         );
    }
}


export default Timer;
