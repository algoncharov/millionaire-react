import React from 'react';
import '../style/poll.css'
 
const HintPoll = ({question2, used, used5050}) => { 

  const Rect = ({x,value, text}) => {
    var Animate = ({value}) => {
      return (
        <animate attributeName="height" from="0" to={used ? value : 0} dur="1s" fill="freeze" />
      )
    }
    return (
      <g>
        <text 
          className="tick"
          x = {x + 10} 
          y = "110%"  
          font-size = "35"
        >
          {text}
        </text>
        <rect 
          x={x} 
          y="0" 
          width="30" 
          height="0" 
          transform="scale(1,-1) translate(0,-200)"
        >
          <Animate value={value*2}/>
        </rect>
      </g>
    )
  }

  console.log(question2.answers[0].percent)
  if (used)
    return(
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
        width="400" height="400" viewBox="0 0 400 200" >
        <g>
          <Rect x =  {50} value={question2.answers[0].percent} text={'A'}/>
          <Rect x =  {90} value={question2.answers[1].percent} text={'B'}/>
          <Rect x = {130} value={question2.answers[2].percent} text={'C'}/>
          <Rect x = {170} value={question2.answers[3].percent} text={'D'}/>
        </g>
        </svg>
      )
  else
    return <div></div>

  // return (
  //           <div style={ {width: '50%'} }> 
  //               <BarChart ylabel={used ? '' : null}
  //                 width={used ? 300 : null}
  //                 height={used ? 300 : null}
  //                 margin={{top: 20, right: 100, bottom: 30, left: 40}}
  //                 data={[ {text: 'A', value: question2.answers[0].percent }, 
  //                         {text: 'B', value: question2.answers[1].percent },
  //                         {text: 'C', value: question2.answers[2].percent },
  //                         {text: 'D', value: question2.answers[3].percent }]}
  //                 // onBarClick={this.handleBarClick}/>
  //                 />
  //           </div>
       
  //   ) 

} 
export default HintPoll



