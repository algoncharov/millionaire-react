import React from 'react';
import Sound from 'react-sound';

// const Sounds = ({url, nextURL}) => {
// 	console.log(nextURL + ' HALO!!!!!!!!!!')
// 	return <Sound
// 		url = {url}
// 		autoLoad = {true}
// 		playStatus = {Sound.status.PLAYING}
// 		loop = {url === bgSound}
// 		onFinishedPlaying={() => {

// 			 nextURL
// 		}}
// 	/>
// }

class Sounds extends React.Component {
	state = {
		url: this.props.url
	}

	componentWillReceiveProps(nextProps){
		this.setState({url:nextProps.url})
	}

	render() {
		console.log(this.state)
		return(
			<Sound 
				url = {this.props.url}
				autoLoad = {true}
				playStatus = {this.props.status || Sound.status.PLAYING}
				volume = {this.props.volume || 100}
				playFromPosition = {0}
				loop = {this.props.loop || false}
				onFinishedPlaying={this.props.onFinishedPlaying}
			/>
		)
	}
}

export default Sounds;
