import React from 'react'

const ButtonTimer = ({fetchQuestionHintCall, used}) => {
	console.log('used '+ used)
  return (
    <li onClick={ !used ? fetchQuestionHintCall : null } className={!used ? 'callfriend': 'usedCallfriend'}></li>
  )
}

export default ButtonTimer
