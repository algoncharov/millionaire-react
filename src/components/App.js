import React        from 'react'
import Root         from './Root'
import PrivateRoute from '../containers/PrivateRoute'
import Login        from '../containers/Login'
import SignUp       from '../containers/SignUp'
import Header       from '../containers/Header'
import Profile      from '../containers/Profile'
import ChangeQuestions from '../containers/admin/ChangeQuestions';
import UserList     from '../containers/admin/UserList';
import Room         from '../containers/room/Room'
import AddQuestion from '../containers/admin/AddQuestion';
import DeleteQuestion from '../containers/admin/DeleteQuestion';
import UpdateQuestion from '../containers/admin/UpdateQuestion';
// import CreatingRoom from '../containers/room/CreatingRoom'
import Public       from './Public'
import Private      from './Private'
import NotFound     from './NotFound'

import {
  Switch,
  Route,
} from 'react-router-dom'

const App = () => {
  return(
    <div>
      <Header />
      <Switch> { /* render 1st matched route */ }
        <PrivateRoute exact path='/' component={Root} />
        <Route path='/login' component={Login} />
        <Route path='/signup' component={SignUp} />
        <Route path='/public' component={Public} />
        <PrivateRoute path='/private' component={Private} />
        <PrivateRoute path='/profile' component={Profile} />
        {/* <PrivateRoute exact path='/room' component={CreatingRoom} /> */}
        <PrivateRoute path='/room/:room_id' component={Room} />
        <PrivateRoute path='/admin/questions' component={ChangeQuestions} />
        <PrivateRoute path='/admin/users' component={UserList} />
        <PrivateRoute path='/admin/newquestion' component={AddQuestion} />
        <PrivateRoute path='/admin/deletequestion' component={DeleteQuestion} />
        <PrivateRoute path='/admin/updatequestion' component={UpdateQuestion} />
        <Route component={NotFound} />
      </Switch>
    </div>
  )
}

export default App
