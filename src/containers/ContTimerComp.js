import { connect } from 'react-redux'
import Timer from '../components/Timer'

const mapStateToProps = state => ({
  used: state.usedTimer,
  start: 30
})
 
export default connect(mapStateToProps)(Timer)
