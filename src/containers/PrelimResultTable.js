import React from 'react';
import { connect } from 'react-redux';
import Answer from '../components/Answer';
import { continuePrelim } from '../actions/game_fetches'
import Sounds from '../components/Sounds'
import prelimEndMuic from '../asserts/music/prelimendmusic.mp3'
import '../style/prelimresulttable.css'

const PrelimResultTable = ({loading,preliminary,prelimCorrectArray,answerDTOs,prelimPlayers,continuePrelim,userId,hostId}) => {
    let delay = 0;
    let orderf = (ansid) => {
        let found = prelimCorrectArray.find(x => x.answerId === ansid)
        return found.ord
    }
    let getDelay = () => {
      let between = 3;
        if (delay === between * 5) {
          delay = 0
        }
        delay += between;
        return delay;
    }
    let letter = String.fromCharCode('A'.charCodeAt()-1);
        return( 
          <div>
             <div className={'answers answertable'}>
                { 
                    answerDTOs.map(ans => <Answer
                        isActive={true}
                        key={ ans.id }
                        text={ ans.text }
                        className={'prelimAnswer'}
                        letter = {letter = String.fromCharCode(letter.charCodeAt()+1)}
                        style={{'order':orderf(ans.id),
                                'animation-delay':getDelay()+'s',
                                  }}
                    />)
                }
               
            </div>
            <div className='prelimPlayerTable' style={{'animation-delay':getDelay()+'s',}}>
              {
                prelimPlayers != null && prelimPlayers.map(player =>
                  <tr key={player.id}  style={player.winner ? {color: '#37a33c'} : {}}>
                    <td>{player.name}</td>
                    <td>{player.surname}</td>
                  </tr>
                )
              }
            </div>  
            {
               (userId === hostId) && <button className='green_button' onClick={continuePrelim}>start game</button>
            }
            <Sounds url = {prelimEndMuic} volume = {50} loop = {false}/>
          </div>

        )
    
}

const mapStateToProps = state => ({
  loading: state.loading,
  preliminary: state.preliminary,
  prelimCorrectArray: state.prelimCorrectArray,
  answerDTOs: state.question.answerDTOs,
  prelimPlayers: state.prelimPlayers,
  userId: state.userId,
  hostId: state.hostId,
})
 
const mapDispatchToProps = dispatch => { 
  return {
    continuePrelim: () => dispatch(continuePrelim()),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(PrelimResultTable);
