import React from 'react'
import { connect } from 'react-redux'
import '../style/profile.css'

const Profile = ({username, role}) => {
  return (
    <div>
      <h1>username: {username}</h1>
      <h1>role: {role}</h1>
    </div>
  )
}

const mapStateToProps = state => { 
    return {
        username: state.username,
        role: state.role,
    } 
}

export default connect(mapStateToProps)(Profile)
