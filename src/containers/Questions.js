import React         from 'react'
import { connect } from 'react-redux'
import { fetchAnswer } from '../actions/game_fetches'
// import QuestionList from '../components/QuestionList'
import Answer from '../components/Answer'
import '../style/question.css'
import { toArrayAnswers } from '../actions/game_fetches'
import { fetchPreliminaryAnswer } from '../actions/game_fetches'
import PrelimResultTable from './PrelimResultTable'
import BgSound       from './BgSound'


const Questions = ({ prelimCorrectArray,hostId, userId, activePlayerId, rezArray, index, fetchPreliminaryAnswer, answerArray, preliminary, used50501, question2, question1, question, show_result, clicked_id, result, fetchAnswer, toArrayAnswers }) => {
  let letter = String.fromCharCode('A'.charCodeAt()-1);

  let classname = function(answer) {
    let isClicked = function(answer) {
      for (var i = 0; i<=answerArray.length-1; i++) {
        if (answer.id === answerArray[i].answerId)
          return true; 
      }
      return false;
    }
    if (show_result && preliminary)
      return isClicked(answer) ? 'wrongAnswer': ''
    else
      return  show_result ? answer.id === clicked_id ||  answer.id === result ? answer.id === result ? 'rightAnswer':'wrongAnswer':'':''
  }

  let answerOnClick =  function(answer) { 
    if (preliminary) {
      toArrayAnswers(answer.id);
      if (index === 3) {
        fetchPreliminaryAnswer();
      }
    } else {
      if(userId === activePlayerId)
        return;
      fetchAnswer(answer.id);
    }
  }

  if (prelimCorrectArray !== null  && preliminary) {
    return (<PrelimResultTable />)
  }
  else if (preliminary && userId === hostId)
    return <div style={{display:'flex',justifyontent:'center'}}>
              <BgSound />
              <h1>Wait until the end of the preliminary stage.</h1>
          </div>
  else{
    return (
      <div className='question_pannel'>
      <BgSound />
        <div className='question'>
          <p>{ question.text }</p>
        </div>
        <div className='answers'> {
          question.answerDTOs.map(answer => <Answer
            userId={userId}
            activePlayerId={activePlayerId}
            isActive={used50501 ? (((answer.id===question1.answerDTOs[0].id) || (answer.id===question1.answerDTOs[1].id)) ? true : false) : true}
            key={ answer.id }
            text={ answer.text }
            letter = {letter = String.fromCharCode(letter.charCodeAt()+1)}
            onClick={() => answerOnClick(answer)}
            className = { classname(answer) + ' answerbar'}
          />
          )
        }
        </div>
      </div>
    )
  }

}


const mapStateToProps = state => ({
  question: state.question,
  question1: state.question1,
  question2: state.question2,
  show_result: state.show_result,
  clicked_id: state.clicked_id,
  result: state.result,
  loading: state.loading,
  used50501: state.used50501,
  preliminary: state.preliminary,
  answerArray: state.answerArray,
  index: state.index,
  rezArray: state.rezArray,
  activePlayerId: state.activePlayerId,
  userId: state.userId,
  hostId: state.hostId,
  prelimCorrectArray: state.prelimCorrectArray,

})

const mapDispatchToProps = dispatch => ({ 
  fetchAnswer: (clicked_id)  => dispatch(fetchAnswer(clicked_id)),
  toArrayAnswers: (clicked_id)  => dispatch(toArrayAnswers(clicked_id)),
  fetchPreliminaryAnswer: () => dispatch(fetchPreliminaryAnswer()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Questions)
