import React from 'react'
import { connect } from 'react-redux'
import { answerClicked } from '../actions/game_fetches'
import Sounds from '../components/Sounds'


const ActionSound = ({url}) => {
	return url != null && <Sounds url = {url}/>
}

const mapStateToProps = state => { 
	console.log('HALO!!!', state.actSound)
    return {
		url: state.actSound,
	} 
}

const mapDispatchToProps = dispatch => { 
    return {
        answerClicked: answerClicked
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionSound)
