import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { signup } from '../actions/auth'
import '../style/login_form.css'

// by now without logout handle 
// 'cause withRouter is hard to learn for now

class SignUp extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      lastname: '',
      password: '',
    }
  }
  
  render() {
    const { from } = { from: { pathname: '/' } }
    if (this.props.login_redirectToReferrer) {
      return <Redirect to={ from } /> 
    }
    else {
      let form_status_class
      let error_message

      if (this.props.login_fail_alert) {
        form_status_class = 'incorrect_form'
        error_message = 'username is token'
      }
      else {
        form_status_class = 'normal_form'
      }

      return (
        <div className={ form_status_class }>

          { error_message }
          
          <form>

            <input  
              type="text"
              placeholder="username"
              onChange={(event) => { 
                this.setState({username: event.target.value})
              }}
            /><br/>

            <input  
              type="text"
              placeholder="lastname"
              onChange={(event) => { this.setState({lastname: event.target.value}) }}
            /><br/>

            <input  
              type="password" 
              placeholder="password" 
              onChange={(event) => { this.setState({password: event.target.value}) }}
            /><br/>

            <button className='white_button' onClick={(event) => {
              event.preventDefault();
              this.props.signup(this.state.username, this.state.lastname, this.state.password)
            }}>Sign Up</button>

          </form>
        </div>
      )
    }
  }
}

const mapStateToProps = state => { 
  return {
    login_fail_alert: state.login_fail_alert,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    signup: (username, lastname, password) => dispatch(signup(username, lastname, password)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
