import React from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import { loadRoomList, fetchRoom } from '../actions/game_fetches'
import newRoom from '../actions/newRoom'
import joinRoom from '../actions/joinRoom'
// import redirectToRoom from '../actions/joinRoom'
import loadRoomListData from '../actions/loadRoomListData'
import '../style/roomList.css'


class RoomList extends React.Component {
  componentWillMount() {
    this.props.loadRoomListData()
  }

  render() {
    console.log('RoomList component before render()')
    if (this.props.roomToRedirect) {
      return <Redirect to={`/room/${this.props.roomToRedirect}`}/>
    }
    else {
      return (
        <div className='roomList'>
          <table>
            <caption>Choose room to play</caption>
            <thead>
              <tr>
                <th>room_id</th>
                <th>players</th>
                <th>room_category</th>
                <th>isActive</th>
                <th><button className='blue_button' onClick={this.props.newRoom}>new room</button></th>
              </tr>
            </thead>
            <tbody>{
              this.props.roomList !== undefined && this.props.roomList.slice(0).reverse().map(room =>
                <tr key={room.id}>
                  <td>{room.id}</td>
                  <td>{room.users.length}</td>
                  <td>{room.category}</td>
                  <td>{room.active? <span>active</span> : <span></span>}</td>
                  <td>{
                    room.id === this.props.joinedRoomId ?
                    <Link to={`/room/${this.props.joinedRoomId}`}><button className='red_button_list'>joined</button></Link>:
                    <Link to={`/room/${room.id}`}><button className='green_button' onClick={() => this.props.joinRoom(room.id)}>join room</button></Link>
                  }</td>
                </tr>
              )
            }
            </tbody>
          </table>
        </div>
      )
    }
  }
}

const mapStateToProps = state => {
  return {
    roomList: state.roomList,
    joinedRoomId: state.joinedRoomId,
    roomToRedirect: state.roomToRedirect,
    sync: state.sync,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    loadRoomList: () => dispatch(loadRoomList()),
    joinRoom: id => dispatch(joinRoom(id)),
    fetchRoom: () => dispatch(fetchRoom()),    
    newRoom: () => dispatch(newRoom()), 
    loadRoomListData: () => dispatch(loadRoomListData()),   
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomList)
