import React from 'react'

const LogOutButton = ({ log_out, history }) => {
  return ( 
    <button className='white_button' onClick={() => {
      log_out()
    }}>Log out</button>
  )
}

export default LogOutButton
