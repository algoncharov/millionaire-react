import React from 'react'
import { connect } from 'react-redux'
import Hint5050 from '../components/Hint5050'
import { fetchQuestionHint5050 } from '../actions/game_fetches'

const Cont5050 = ({ fetchQuestionHint5050, question, used}) => (
    <Hint5050 fetchQuestionHint5050={ fetchQuestionHint5050 } used = {used}/>
)

const mapStateToProps = state => ({
  question: state.question,
  used: state.used5050,
})

const mapDispatchToProps = dispatch => ({ 
  fetchQuestionHint5050: () => dispatch(fetchQuestionHint5050()) 
})

export default connect(mapStateToProps, mapDispatchToProps)(Cont5050)


