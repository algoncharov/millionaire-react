import React 		 from 'react';
import { connect }   from 'react-redux';

const StatusBar = ({isActive}) => {
	if (isActive)
		return (<div className = "wall"></div>)
	else
		return null
}

const mapStateToProps = state => ({
  isActive: (state.userId === state.activePlayerId)
})

export default connect(mapStateToProps)(StatusBar)