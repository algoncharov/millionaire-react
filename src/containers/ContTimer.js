import React from 'react'
import { connect } from 'react-redux'
import ButtonTimer from '../components/ButtonTimer'
import { fetchQuestionHintCall } from '../actions/game_fetches'

const ContTimer = ({ fetchQuestionHintCall, used }) => (
    <ButtonTimer fetchQuestionHintCall={ fetchQuestionHintCall } used = {used}/>
)

const mapStateToProps = state => ({
  used: state.usedTimer1
})

const mapDispatchToProps = dispatch => ({ 
  fetchQuestionHintCall: () => dispatch(fetchQuestionHintCall()) 
})

export default connect(mapStateToProps, mapDispatchToProps)(ContTimer)
