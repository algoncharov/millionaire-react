import React from 'react'
import { connect } from 'react-redux'
import { answerClicked } from '../actions/game_fetches'
import Score from '../components/Score'


const FetchScore = ({active}) => {
	return <Score active = {active}/>
}

const mapStateToProps = state => { 
	console.log('BUUUUUUUUUUUUU', state)
    return {
		active: state.question.id
	} 
}

const mapDispatchToProps = dispatch => { 
    return {
        answerClicked: answerClicked
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FetchScore)
