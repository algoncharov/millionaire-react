import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { playAgain } from '../actions/game_fetches'

const Reset = ({ playAgain}) => (
  <Link to='/game' onClick={ playAgain }>New Game</Link>
)

const mapDispatchToProps = dispatch => ({ 
  playAgain: () => dispatch(playAgain(1)) 
})

export default connect(null, mapDispatchToProps)(Reset)
