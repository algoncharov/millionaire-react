import React from 'react'
import { connect } from 'react-redux'
import WaitGame from './WaitGame'
import Game from './Game'

const Room = props => {
  console.log('TYGA')  
  // if (parseInt(props.match.params.room_id, 10) === props.joinedRoomId) { 
    console.log('props.gameStarted', props.gameStarted)
    if (props.gameStarted) {
      return <Game roomId={ props.match.params.room_id } />
    }
    else {
      return <WaitGame roomId={props.match.params.room_id} />
    }
  // }
  // else {
    // return <Redirect to='/'/>
  // }
}

const mapStateToProps = state => {
  return {
    joinedRoomId: state.joinedRoomId,
    gameStarted: state.gameStarted,
  }
}

export default connect(mapStateToProps)(Room)
