import React         from 'react'
import { connect }   from 'react-redux'
import Question      from '../Questions'
import ActionSound   from '../ActionSound'
import ContHintPoll  from '../ContHintPoll'
import ContTimerComp from '../ContTimerComp'
import Spinner       from '../Spinner'
import WSWrapper     from '../WSWrapper'
import StatusBar     from '../StatusBar'
import Wall          from '../Wall'
import                    '../../style/game.css'
import Status        from '../Status'

const Game = props => {
  return(
    <div className='game'>
      <Wall/>
      <ActionSound />
      <StatusBar/>
      <Question />
      <ContHintPoll />
      <ContTimerComp />
      <WSWrapper roomId={ props.roomId }/>
      <Status/>
      <Spinner />
    </div>
  )
}

const mapStateToProps = state => {
  return {
    roomList: state.roomList,
  }
}

export default connect(mapStateToProps)(Game)
