import React from 'react'
import '../../style/waitGame.css'
import { connect } from 'react-redux'
import startGameServer from '../../actions/startGame'
import WSWrapper     from '../WSWrapper'
import loadRoomListData from '../../actions/loadRoomListData'
import { syncComplete } from '../../actions/sync'
import { fetchOneRoom } from '../../actions/game_fetches'
// import Reset      from '../containers/Reset'

class WaitGame extends React.Component {
  
  componentWillMount() {
    this.props.fetchOneRoom(this.props.roomId);
    // this.props.loadRoomListData()
  }

  render() {
    console.log('roomUserList-roomUserList-roomUserList', this.props.roomUserList)
    if (this.props.roomUserList != null && this.props.roomUserList.length > 0) {
      return (
        <div className='waitGame'>
          <table>
            <caption>Room: {this.props.roomId}<br/> Wait Game Screen.<br/></caption>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Surname</th>
                  <th>Host</th>
              </tr>
            </thead>
            <tbody> {
              this.props.roomUserList.map(user =>
                <tr key={user.id}  style={user.id === this.props.userId ? {color: '#37a33c'} : {}}>
                  <td>{user.name}</td>
                  <td>{user.surname}</td>
                  <td>{user.id === this.props.hostId ? '✓' : ''}</td>
                </tr>
                )
              }
              </tbody>
            </table>
            {this.props.userId === this.props.hostId &&
              <button className='green_button' onClick={() => { 
                console.log(this.props.roomId)
                this.props.startGame(this.props.roomId) 
              }}>start game</button>
            }
          <WSWrapper roomId={ this.props.roomId } />
        </div>
      )
    }
    else {
      return <h1>loading...</h1>
    }
  }
}

const mapStateToProps = state => {
  return {
    roomUserList: state.roomUserList,
    joinedRoomId: state.joinedRoomId,
    activeRoom: state.activeRoom,
    userId: state.userId,
    hostId: state.hostId,
    sync: state.sync,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    startGame: roomId => dispatch(startGameServer(roomId)),
    loadRoomListData: () => dispatch(loadRoomListData()),
    syncComplete: () => dispatch(syncComplete()),
    fetchOneRoom: roomId =>dispatch(fetchOneRoom(roomId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WaitGame)