import React from 'react'
import '../../style/roomList.css'

//TODO: move to components

export class UserRow extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.user.id,
      name: this.props.user.name,
      surname:this.props.user.surname,
      userRoleId:this.props.user.userRoleId,
      clicked: false,
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleActivateRow = this.handleActivateRow.bind(this);
    this.handleUpdateUser = this.handleUpdateUser.bind(this);
    this.handleBanUser = this.handleBanUser.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked ? 1 : 2: target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
    console.log('state', this.state)

  }
  handleActivateRow() {
    this.setState({
      clicked: !this.state.clicked,
    });
    console.log('clicked', this.state)
  }
  handleUpdateUser() {
    this.setState({
      clicked: !this.state.clicked,
    });
    this.props.updateUser(this.state.id, this.state);
    
  }

  handleBanUser() {
    this.setState({
      userRoleId: this.state.userRoleId === 3 ? 2 : 3,
    })
    this.handleUpdateUser();
    console.log(this.state)
  }

  render() {
    if (this.state.clicked) 
      return (
        <tr className='row' >
          <td>
          <input  
              type="text"
              placeholder="name"
              name="name"
              value={this.state.name}
              onChange={this.handleInputChange}
            />
          </td>
          <td>
            <input  
              type="text"
              name="surname"
              placeholder="surname"
              value={this.state.surname}
              onChange={this.handleInputChange}
            />
            <br/>
          </td>
          <td>
            <input 
              type="checkbox" 
              name="userRoleId"
              checked={this.state.userRoleId === 1 ? "checked": ''}
              onChange={this.handleInputChange}
            />
          </td>
          <td>
            <button 
              onClick = {this.handleBanUser}
              className='white_button'
            >
              {this.state.userRoleId === 3 ? 'unban' : 'ban'}
            </button>
          </td>
          <td>
            <button 
              onClick = {this.handleUpdateUser}
              className='white_button'
            >
              update
            </button>
          </td>
          <td>
            <button 
              onClick = {() => this.props.deleteUser(this.state.id)} 
              className='red_button_header'
            >
              delete
            </button>
          </td>
        </tr>
      )
      else 
        return (
          <tr onClick = {this.handleActivateRow} className='smpleRow'>
            <td>
              <div>{this.state.name}</div>
            </td>

            <td>
              <div>{this.state.surname}</div>
            </td>

            <td>
                <input 
                  type="checkbox" 
                  name="userRoleId"
                  checked={this.state.userRoleId === 1 ? "checked": ''}
                  onChange={this.handleInputChange}
                />
            </td>

            <td>
              <button 
                onClick = {this.handleBanUser}
                className='white_button'
              >
                {this.state.userRoleId === 3 ? 'unban' : 'ban'}
              </button>
            </td>

            <td>
                <button 
                  onClick = {this.handleUpdateUser}
                  className='white_button'
                >
                  update
                </button>
            </td>

            <td>
              <button 
                onClick = {() => this.props.deleteUser(this.state.id)} 
                className='red_button_header'
              >
                delete
              </button>
            </td>
          </tr>
    )
  }
}
