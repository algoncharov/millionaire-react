import React from 'react'
import { connect } from 'react-redux'
import { saveQuestion, getCategory } from '../../actions/admin'
import '../../style/login_form.css'
import { Link } from 'react-router-dom'







class AddQuestion extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      category: '',
      question: '',
      answer1: '',
      answer2: '',
      answer3: '',
      answer4: '',
    }
  }

  componentDidMount() {
    this.props.getCategory()
    console.log(this.state)
  }
  
  render() {
    return (
        <div>
        <Link to='/admin/questions'><button className='green_button'>Назад</button></Link>
        <div className={ 'normal_form' }>
          
       

          <form>
          {console.log(this.state)}
          <select onChange={(event) => { this.setState({category: event.target.value}) }} >
            {this.props.categoryesList1.map(category => 
              <option value={category.ord} >Категория: {category.ord===-1 ? 'отборочный' : category.ord+1}</option>
            )}
          </select>
          <br/>
            <input  
              type="text"
              placeholder="question"
              onChange={(event) => { this.setState({question: event.target.value}) }}
            />
            <br/>
            <input  
              type="text" 
              placeholder="true answer" 
              onChange={(event) => { this.setState({answer1: event.target.value}) }}
            />
            <br/>
            <input  
              type="text" 
              placeholder="false answer" 
              onChange={(event) => { this.setState({answer2: event.target.value}) }}
            />
            <br/>
            <input  
              type="text" 
              placeholder="false answer" 
              onChange={(event) => { this.setState({answer3: event.target.value}) }}
            />
            <br/>
            <input  
              type="text" 
              placeholder="false answer" 
              onChange={(event) => { this.setState({answer4: event.target.value}) }}
            />
            <br/>
            <button className='white_button' onClick={(event) => {
              event.preventDefault();
              this.props.saveQuestion(this.state.category, this.state.question, this.state.answer1, this.state.answer2, this.state.answer3, this.state.answer4)
            }}>Save</button>
          </form>
        </div>
        </div>
        )
    }
  }


const mapStateToProps = state => { 
  return {
    categoryesList: state.categoryesList,
    categoryesList1: state.categoryesList1,
    login_fail_alert: state.login_fail_alert,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    saveQuestion: (category, question, answer1, answer2, answer3, answer4) => dispatch(saveQuestion(category, question, answer1, answer2, answer3, answer4)),
    getCategory: () => dispatch(getCategory())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddQuestion)

