import React from 'react'
import '../../style/login_form.css'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { redirectToReferrerFalse } from '../../actions/auth'


import { Link } from 'react-router-dom';

const ChangeQuestions = () => {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    if (this.props.login_redirectToReferrer) {
      this.props.redirectToReferrerFalse()
      return <Redirect to={ from } /> 
    } 
    else 
    return (
		<div>
			<Link to='/admin/newquestion'><button className='green_button'>Create new question</button></Link>
			<Link to='/admin/deletequestion'><button className='green_button'>Delete question</button></Link>
		</div>
	)
}
const mapStateToProps = state => {
  return {
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    redirectToReferrerFalse: () => dispatch(redirectToReferrerFalse()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ChangeQuestions)


