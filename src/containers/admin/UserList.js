import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { loadUserList, deleteUser, updateUser, updatedUser} from '../../actions/admin'
import { redirectToReferrerFalse } from '../../actions/auth'
import '../../style/roomList.css'
import { UserRow } from './UserRow'
import  RingLoader from '../../components/RingLoader'


class UserList extends React.Component {

  componentDidMount() {
    this.props.loadUserList()
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    console.log('Login.render() from: ',from, 'props.login_redirectToReferrer', this.props.login_redirectToReferrer)
    if (this.props.login_redirectToReferrer) {
      this.props.redirectToReferrerFalse()
      return <Redirect to={ from } /> 
    }
    else 
      if (this.props.userList.length === 0) 
        return <RingLoader color={'#eee'} />
      else
        return (
          <div className='adminTable'>
            <table>
              <caption>Users</caption>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Surname</th>
                  <th>Admin</th>
                  <th>Ban</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>{
                this.props.userList.map(user =>
                  <UserRow 
                    user={user} 
                    key={user.id}
                    deleteUser = {this.props.deleteUser}
                    updateUser = {this.props.updateUser}
                  />
                )
              }
              </tbody>
            </table>
          </div>
        )
  }
}

const mapStateToProps = state => {
  return {
    userList: state.userList,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    loadUserList: () => dispatch(loadUserList()),
    deleteUser:  id => dispatch(deleteUser(id)),
    updateUser: (id, user) => dispatch(updateUser(id, user)),
    updatedUser: user => dispatch(updatedUser(user)),
    redirectToReferrerFalse: () => dispatch(redirectToReferrerFalse()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)
