import React from 'react'
import { Redirect } from 'react-router-dom'
import { redirectToReferrerFalse } from '../../actions/auth'
import { connect } from 'react-redux'
import { getCategory, updateQuest } from '../../actions/admin'
import '../../style/login_form.css'
import { Link } from 'react-router-dom'

class UpdateQuestion extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.questionUpdateid.id,
      category: this.props.questionUpdateid.category,
      question: this.props.questionUpdateid.text,
      answer1: this.props.questionUpdateid.answer1,
      answer2: this.props.questionUpdateid.answer2,
      answer3: this.props.questionUpdateid.answer3,
      answer4: this.props.questionUpdateid.answer4,
    }
  }

  componentDidMount() {
    this.props.getCategory()
    console.log(this.state)
  }
  
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    if (this.props.login_redirectToReferrer) {
      this.props.redirectToReferrerFalse()
      return <Redirect to={ from } /> 
    } 
    else 
      return (
          <div>
          <Link to='/admin/questions'><button className='green_button'>Назад</button></Link>
          <div className={ 'normal_form' }>

            <form>
            {console.log(this.state)}
          
            
              <input  
                type="text"
                placeholder="question"
                value={this.state.question}
                onChange={(event) => { this.setState({question: event.target.value}) }}
              />
              <br/>
              <input  
                type="text" 
                placeholder="true answer" 
                value={this.state.answer1}
                onChange={(event) => { this.setState({answer1: event.target.value}) }}
              />
              <br/>
              <input  
                type="text" 
                placeholder="false answer" 
                value={this.state.answer2}
                onChange={(event) => { this.setState({answer2: event.target.value}) }}
              />
              <br/>
              <input  
                type="text" 
                placeholder="false answer" 
                value={this.state.answer3}
                onChange={(event) => { this.setState({answer3: event.target.value}) }}
              />
              <br/>
              <input  
                type="text" 
                placeholder="false answer" 
                value={this.state.answer4}
                onChange={(event) => { this.setState({answer4: event.target.value}) }}
              />
              <br/>
              <button className='white_button' onClick={(event) => {
                event.preventDefault();
                this.props.updateQuest(this.state.id, this.state.category, this.state.question, this.state.answer1, this.state.answer2, this.state.answer3, this.state.answer4)
              }}>Save</button>
            </form>
          </div>
          </div>
          )
    }
  }


const mapStateToProps = state => { 
  return {
    categoryesList: state.categoryesList,
    questionUpdateid: state.questionUpdateid,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    updateQuest: (id, category, question, answer1, answer2, answer3, answer4) => dispatch(updateQuest(id, category, question, answer1, answer2, answer3, answer4)),
    getCategory: () => dispatch(getCategory()),
    redirectToReferrerFalse: () => dispatch(redirectToReferrerFalse()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateQuestion)

