import React from 'react';
import { Link } from 'react-router-dom';
import '../../style/admin.css'

const Admin = () => {
	return (
		<div className='adminRoom'>
			<Link to='/admin/questions'><button className='green_button'>Manage questions</button></Link>
			<Link to='/admin/users'><button className='green_button'>Manage users</button></Link>
		</div>
	)
}

export default Admin
