import React from 'react'
import { Redirect } from 'react-router-dom'
import { redirectToReferrerFalse } from '../../actions/auth'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { loadQuestionList, deleteQuest, updatedQuestion } from '../../actions/admin'
import '../../style/roomList.css'



class DeleteQuestion extends React.Component {
  componentDidMount() {
    this.props.loadQuestionList()
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    if (this.props.login_redirectToReferrer) {
      this.props.redirectToReferrerFalse()
      return <Redirect to={ from } /> 
    } 
    else 
    return (
      <div>
      <Link to='/admin/questions'><button className='green_button'>Назад</button></Link>
      <div className='roomList'>
        <table>
          <caption>Choose question to remove</caption>
          <thead>
            <tr>
              <th>question_id</th>
              <th>text</th>
              <th>question_category</th>
            </tr>
          </thead>
          <tbody>{
            this.props.questionList.map(question =>
              <tr key={question.id}>
                <td>{question.id}</td>
                <td>{question.text}</td>
                <td>{question.ord}</td>
                <th><button className='white_button' onClick={() => this.props.deleteQuest(question.id)}>remove</button></th>
                <th><Link to='/admin/updatequestion'><button className='white_button' onClick={() => this.props.updatedQuestion(question.id, question.ord, question.text, question.answerDTOs[0].text, question.answerDTOs[1].text, question.answerDTOs[2].text, question.answerDTOs[3].text)}>update</button></Link></th>
              </tr>
            )

          }
          </tbody>
        </table>
      </div>
      </div>
    )
  }
}



const mapStateToProps = state => {
  return {
    questionList: state.questionList,
    activeRoom: state.activeRoom,
    login_redirectToReferrer: state.login_redirectToReferrer,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    loadQuestionList: () => dispatch(loadQuestionList()),
    deleteQuest: (questionId) => dispatch(deleteQuest(questionId)),  
    updatedQuestion: (id, category, question, answer1, answer2, answer3, answer4) => dispatch(updatedQuestion(id, category, question, answer1, answer2, answer3, answer4)),
    redirectToReferrerFalse: () => dispatch(redirectToReferrerFalse()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteQuestion)
 