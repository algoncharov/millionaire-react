import React from 'react'
import { connect } from 'react-redux'
import { requestPosts } from '../actions/game_fetches'
import Sounds from '../components/Sounds'


const BgSound = ({url,preliminary,requestPostsd}) => { 
	return <Sounds url = {url} volume = {50} loop = {!preliminary} onFinishedPlaying={preliminary ? requestPostsd : () => {}}/>
}

const mapStateToProps = state => { 
    return {
		url: state.bgSound,
        preliminary: state.preliminary,
	} 
}

// по идее не юзается, TODO: delete
const mapDispatchToProps = dispatch => { 
    return {
        requestPostsd: () => dispatch(requestPosts())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BgSound)
