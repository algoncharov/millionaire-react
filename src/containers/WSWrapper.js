import { connect }                      from 'react-redux'
import { fetchOneRoom, 
        fetchQuestion, 
        answerClicked, 
        preliminaryAnswerClicked,
        receivePostsHint5050,
        receivePostsHintPoll,
        receivePostsHintCall,
        setPrelimResults,
        getOneRoom           }          from '../actions/game_fetches'
import {startGame}                      from '../actions/startGame'
import { rest_api_host }                from '../index'
import React                            from 'react';
import SockJsClient                     from 'react-stomp';

const WSWrapper = props => { 
    return (
      <div>
        <SockJsClient url={rest_api_host + '/gs-guide-websocket'} topics={['/topic/room' + props.joinedRoomId]}
            onMessage={(msg) => { 
                if (!props.isFetching) {
                    switch (msg.wsMessage) {
                        case 'NEXTQUESTION':
                            props.answerClicked(msg.answerId,msg.correct);
                            setTimeout(() => props.fetchQuestion(), 5000);
                            break;
                        case 'ENDPRELIMQUESTION':
                            props.setPrelimResults(msg);
                            //props.preliminaryAnswerClicked(msg.answerPairs,msg.correctAnswerPairs);
                            props.fetchOneRoom(props.roomId);
                            break;
                        case 'CONTINUEPRELIMQUESTION':
                            setTimeout(() => props.fetchQuestion());
                            break;
                        case 'HINT5050USED':
                            props.receivePostsHint5050(msg.payload);
                            break;
                        case 'HINTPOLLUSED':
                            props.receivePostsHintPoll(msg.payload);
                            break;
                        case 'HINTCALLUSED':
                            props.receivePostsHintCall();
                            break;
                        case 'GAMESTART':
                            props.fetchQuestion()
                            props.startGame()
                            break;
                        case 'ROOMSTATECHANGED':
                            props.getOneRoom(msg.payload)
                            break;
                        default:
                            break;
                    }
                  
                }
             }}
            debug={true}/>
      </div>    
    );
  }



const mapStateToProps = state => ({
    isFetching: state.isFetching,
    joinedRoomId: state.joinedRoomId,
})

const mapDispatchToProps = dispatch => ({ 
  fetchQuestion: () => dispatch(fetchQuestion()),
  startGame:() => dispatch(startGame()),
  fetchOneRoom: roomId => dispatch(fetchOneRoom(roomId)),
  answerClicked: (clicked_id, result) => dispatch(answerClicked(clicked_id, result)),
  receivePostsHint5050: (question) => dispatch(receivePostsHint5050(question)),
  receivePostsHintPoll: (question) => dispatch(receivePostsHintPoll(question)),
  receivePostsHintCall:() => dispatch(receivePostsHintCall()),
  getOneRoom:(room) => dispatch(getOneRoom(room)),
  preliminaryAnswerClicked: (answerArray, correctAnswerPairs) => {
    dispatch(preliminaryAnswerClicked(answerArray,correctAnswerPairs))
  },
  setPrelimResults:(arr) => dispatch(setPrelimResults(arr))

  
})

export default connect(mapStateToProps,mapDispatchToProps)(WSWrapper)
