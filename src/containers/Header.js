import React      from 'react';
import LogOutButton from './LogOutButton'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { log_out } from '../actions/auth'
import '../style/header.css'

const Header = ({logged_in, log_out, username, joinedRoomId, gameStarted, history}) => {
  if (logged_in) {
    return(
      <header>
        <Link to='/'><button className='white_button'>Main page</button></Link>
        {
          joinedRoomId ?
            gameStarted ?
              <Link to={`/room/${joinedRoomId}`}>
                <button className='red_button_header'>{`room ${joinedRoomId} | game`}</button>
              </Link>
            :
              <Link to={`/room/${joinedRoomId}`}>
                <button className='red_button_header'>{`room ${joinedRoomId} | waiting for game`}</button>
              </Link>
          :
            <span></span>
        }
        <span>logged in as <Link to='profile'>{ username }</Link></span>
        <LogOutButton log_out={ log_out }/>
      </header>
    )
  }
  else {
    return (
      <header>
        <Link to='/login'><button className='white_button'>Login</button></Link>
        <Link to='/signup'><button className='white_button'>Sign Up</button></Link>
      </header>
    )
    
  }
}


const mapStateToProps = state => {
  return {
    logged_in: state.logged_in,
    username: state.username,
    joinedRoomId: state.joinedRoomId,
    gameStarted: state.gameStarted,
  }
}

const mapDispatchToProps = dispatch => { 
  return {
    log_out: () => dispatch(log_out())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
