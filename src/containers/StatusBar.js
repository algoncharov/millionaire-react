import React 		 from 'react';
import { connect }   from 'react-redux';
import FetchScore    from '../containers/FetchScore'
import Lifelines     from '../components/Lifelines'

const StatusBar = ({isPreliminary,hostId,activePlayerId,userId}) => {
	if (isPreliminary)
		return <div></div>
	else if (userId === hostId || userId === activePlayerId) {
		return (
	      <div className = "status">    		
	        <Lifelines />
	        <FetchScore />
	      </div>
	    )
	} else {
		return (
	      <div className = "status">
	        <FetchScore />
	      </div>
	    )
	}
}

const mapStateToProps = state => ({
  isPreliminary: state.preliminary,
  hostId: state.hostId,
  activePlayerId: state.activePlayerId,
  userId: state.userId
})

export default connect(mapStateToProps)(StatusBar)