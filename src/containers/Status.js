import React from 'react';
import { connect } from 'react-redux'

const Status = ({status}) => 
<p className='playerStatus'>{'status: ' + status}</p>

const mapStateToProps = state => ({
  status: state.userId === state.activePlayerId ? 'active':  state.userId === state.hostId?'host':'player' 
})

export default connect(mapStateToProps)(Status)
